# staticcheck


## TL;DR;

We build container image for the static check tool for golang project provided and maintained at https://github.com/dominikh/go-tools

```
docker run -it -v $PWD:/workspace registry.gitlab.com/etangs/staticcheck sh -c "cd /workspace && staticcheck ./..."
```
